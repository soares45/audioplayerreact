import * as HttpStatus from "http-status-codes";
import { push } from "react-router-redux";
import { Dispatch } from "redux";
import baseUrl from "constants";

import axios, { AxiosError, AxiosPromise, AxiosRequestConfig, AxiosResponse } from "axios";

import { REQUEST_PREFIX } from "./constant";
import { IAsyncAction } from "./model";

export function executeGetRequest(url: string, queryParams?: { [key: string]: string }): AxiosPromise<any> {
    return axios.get(buildEndpointWithParameters(url, queryParams), buildAxiosConfig());
}

export function executePostRequest(url: string, body: any, queryParams?: { [key: string]: string }): AxiosPromise<any> {
    return axios.post(buildEndpointWithParameters(url, queryParams), body, buildAxiosConfig());
}

export function executePutRequest(url: string, body: any, queryParams?: { [key: string]: string }): AxiosPromise<any> {
    return axios.put(buildEndpointWithParameters(url, queryParams), body, buildAxiosConfig());
}

export function executeDeleteRequest(url: string, queryParams?: { [key: string]: string }): AxiosPromise<any> {
    return axios.delete(buildEndpointWithParameters(url, queryParams), buildAxiosConfig());
}

export function executeLoginRequest(url: string, body: any, queryParams?: { [key: string]: string }): AxiosPromise<any> {
    return axios.post(buildEndpointWithParameters(url, queryParams), body);
}

export function createAsyncAction(actionTypePrefix: string, asyncAction: IAsyncAction) {
    if (!asyncAction.types[actionTypePrefix.concat(REQUEST_PREFIX.START)]) {
        console.error(
            "Your action-type doesn't respect this project rules. Verify : " + actionTypePrefix + " : ",
            Object.keys(asyncAction.types));
    }

    return (dispatch: Dispatch) => {
        dispatch(buildBaseAction(actionTypePrefix.concat(REQUEST_PREFIX.START))); // dispatch start request
        return asyncAction.requestAction(dispatch)
            .then((response: AxiosResponse<any>) => {
                dispatch(buildBaseAction(actionTypePrefix.concat(REQUEST_PREFIX.SUCCESS), response)); // dispatch success request

                if (asyncAction.successAction) {
                    asyncAction.successAction(dispatch, response.data, response); // dispatch asyncAction.successAction
                }
            })
            .catch((error: AxiosError) => {
                console.error("Error during action : " + actionTypePrefix, error.config);
                if (error.response && error.response.status === HttpStatus.UNAUTHORIZED) {
                    dispatch(push("/"));
                }
                dispatch(buildBaseAction(actionTypePrefix.concat(REQUEST_PREFIX.FAILURE), error.response)); // dispatch error request

                if (asyncAction.errorAction) {
                    asyncAction.errorAction(dispatch, error.response || buildDefaultErrorResponse()); // dispatch asyncAction.errorAction
                }
            });

    };
}

const buildAxiosConfig = (): AxiosRequestConfig => {
    return {
        headers: {
            "Content-Type": "application/json"
        },
        withCredentials: true
    };
};

const buildBaseAction = (actionType: string, response?: AxiosResponse<any>) => {
    return {
        type: actionType,
        payload: response
    };
};

const buildEndpointWithParameters = (endpoint: string, params?: { [key: string]: string }): string => {
    if (!params) {
        return baseUrl + endpoint;
    }
    const resultArray: string[] = [];
    Object.keys(params).forEach((key: string) => {
        let value: string = params[key];
        value = value.trim();
        resultArray.push(key + "=" + encodeURIComponent(value));
    });
    return baseUrl + endpoint + "?" + resultArray.join("&");
};

const buildDefaultErrorResponse = (): AxiosResponse<any> => {
    return {
        data: {},
        status: HttpStatus.IM_A_TEAPOT,
        statusText: "Unknown error response",
        headers: {},
        config: {}
    };
};